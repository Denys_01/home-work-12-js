const buttons = document.querySelectorAll('.btn');
let currentButton = null;
function toggleColor(button){
    if(currentButton !== null){
        currentButton.style.backgroundColor='black';
    }
     if(currentButton !== button){
        button.style.backgroundColor ='blue';
        currentButton = button;
    }else{
        currentButton = null;
    }
}
function keyDown(event){
    const key = event.key.toUpperCase();
    const button = Array.from(buttons).find(b => b.textContent.toUpperCase()=== key);
    toggleColor(button);
}
document.addEventListener('keydown',keyDown);







